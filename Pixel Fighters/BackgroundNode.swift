//
//  MachineNode.swift
//  Pixel Fighters
//
//  Copyright (c) 2015 Rishabh Parikh. All rights reserved.
//

import SpriteKit




class BackgroundNode: SKSpriteNode {
    
    
    
    let levelBgImage = "mountain_range"
    
    
    init(backgroundAtPosition: CGPoint) {
        
        
        
        
        
        
        var texture = SKTexture(imageNamed: levelBgImage)
        

        
        super.init(texture: texture, color: UIColor.whiteColor(), size: texture.size())
     
        
        
        

        
        
        self.name = "Background"

        self.anchorPoint = CGPoint(x: 0, y: 0)
        self.size.width = self.frame.size.width
        self.size.height = self.frame.size.height

        
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    
    
    
    
    func backgroundGo() {
        
        var texture = SKTexture(imageNamed: levelBgImage)
        
        //make your SKActions that will move the image across the screen. this one goes from right to left.
        var moveBackground = SKAction.moveByX(-texture.size().width, y: 0, duration: NSTimeInterval(0.005 * texture.size().width))
        
        //This resets the image to begin again on the right side.
        var resetBackGround = SKAction.moveByX(texture.size().width, y: 0, duration: 0.0)
        
        //this moves the image run forever and put the action in the correct sequence.
        var moveBackgoundForever = SKAction.repeatActionForever(SKAction.sequence([moveBackground, resetBackGround]))
        
        
        for var i:CGFloat = 0; i<2 + self.frame.size.width / (texture.size().width); ++i {
            var background = SKSpriteNode(texture: texture)
            background.position = CGPointMake(i * background.size.width, background.size.height / 2)
            background.runAction(moveBackgoundForever)
            self.addChild(background)
        }
        
    }
    
    
    

    
    
}

