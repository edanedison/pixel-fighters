//
//  SpaceDog.swift
//  Pixel Fighters
//
//  Created by Rishabh Parikh on 1/12/15.
//  Copyright (c) 2015 Rishabh Parikh. All rights reserved.
//

import SpriteKit

enum SpaceDogType: Int {
    case SpaceDogTypeA = 0
    case SpaceDogTypeB = 1
}

class SpaceDogNode: SKSpriteNode {
    var texturesA: [SKTexture]
    var texturesB: [SKTexture]
    var health = 0
    var spaceDogType: SpaceDogType

    func checkHealth() {
        if(self.health == 0) {
            if(self.spaceDogType == SpaceDogType.SpaceDogTypeA) {
                self.texture = SKTexture(imageNamed: "rock1")
                self.removeActionForKey("AnimationA")
            }
            else {
                self.texture = SKTexture(imageNamed: "rock2")
                self.removeActionForKey("AnimationB")
            }
        } else if self.health < 0 {
            self.removeFromParent()
        }
    }

    init(dogAtPosition: CGPoint, spaceDogType: SpaceDogType) {

        var texture: SKTexture

        texturesA = [SKTexture(imageNamed: "rock1"), SKTexture(imageNamed: "rock1")]
        texturesB = [SKTexture(imageNamed: "rock2"), SKTexture(imageNamed: "rock2"),
                     SKTexture(imageNamed: "rock2")]

        var dogAnimationA = SKAction.animateWithTextures(texturesA, timePerFrame: 0.1)
        var dogAnimationB = SKAction.animateWithTextures(texturesB, timePerFrame: 0.1)

        if spaceDogType == SpaceDogType.SpaceDogTypeA {
            self.spaceDogType = SpaceDogType.SpaceDogTypeA
            texture = SKTexture(imageNamed: "rock1")
            super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
            self.runAction(SKAction.repeatActionForever(dogAnimationA), withKey: "AnimationA")
        }
        else {
            self.spaceDogType = SpaceDogType.SpaceDogTypeB
            texture = SKTexture(imageNamed: "rock2")
            super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
            self.runAction(SKAction.repeatActionForever(dogAnimationB), withKey: "AnimationB")
        }
        setupPhysicsBody()

        var scale: CGFloat = CGFloat(Util.randomNumberWithinRange(50, max: 101)) / 100.0
        self.xScale = scale
        self.yScale = scale
        self.zPosition = 30

        self.position = dogAtPosition
    }
    
    

    init(spaceDogType: SpaceDogType) {
        var texture: SKTexture
            println("add enemy")
        texturesA = [SKTexture(imageNamed: "rock1"), SKTexture(imageNamed: "rock1")]
        texturesB = [SKTexture(imageNamed: "rock2"), SKTexture(imageNamed: "rock2"),
                     SKTexture(imageNamed: "rock2")]

        var dogAnimationA = SKAction.animateWithTextures(texturesA, timePerFrame: 0.1)
        var dogAnimationB = SKAction.animateWithTextures(texturesB, timePerFrame: 0.1)

        if spaceDogType == SpaceDogType.SpaceDogTypeA {
            self.spaceDogType = SpaceDogType.SpaceDogTypeA
            texture = SKTexture(imageNamed: "rock1")
            super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
            self.runAction(SKAction.repeatActionForever(dogAnimationA), withKey: "AnimationA")
        }
        else {
            self.spaceDogType = SpaceDogType.SpaceDogTypeB
            texture = SKTexture(imageNamed: "rock2")
            super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
            self.runAction(SKAction.repeatActionForever(dogAnimationB), withKey: "AnimationB")
        }
        setupPhysicsBody()
        var scale: CGFloat = CGFloat(Util.randomNumberWithinRange(50, max: 101)) / 100.0
        self.xScale = scale
        self.yScale = scale
        self.zPosition = 30
    }

    func setupPhysicsBody() {
        self.physicsBody = SKPhysicsBody(rectangleOfSize: self.frame.size)
        self.physicsBody?.affectedByGravity = true
        self.physicsBody?.velocity = CGVectorMake(0, -50)
        self.physicsBody?.dynamic = true // 2
        
        
        self.physicsBody?.categoryBitMask = CollisionCategory.Rock.rawValue
        self.physicsBody?.collisionBitMask = CollisionCategory.Projectile.rawValue|CollisionCategory.Rock.rawValue;
        self.physicsBody?.contactTestBitMask = CollisionCategory.Projectile.rawValue | CollisionCategory.Ground.rawValue
        
        

        self.physicsBody?.allowsRotation = true

        self.physicsBody!.mass = 1
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
