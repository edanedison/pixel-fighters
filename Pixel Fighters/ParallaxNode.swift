//
//  MachineNode.swift
//  Pixel Fighters
//
//  Copyright (c) 2015 Rishabh Parikh. All rights reserved.
//

import SpriteKit




class ParallaxNode: SKSpriteNode {
    
    
    
    let parallaxImage = "mountain_clouds"
    
    init(parallaxAtPosition: CGPoint) {
        
        
        
        
        
        
        var texture = SKTexture(imageNamed: parallaxImage)
        

        
        super.init(texture: texture, color: UIColor.whiteColor(), size: texture.size())
     
        
        //make your SKActions that will move the image across the screen. this one goes from right to left.
        var moveParallax = SKAction.moveByX(-texture.size().width, y: 0, duration: NSTimeInterval(0.02 * texture.size().width))
        
        //This resets the image to begin again on the right side.
        var resetParallax = SKAction.moveByX(texture.size().width, y: 0, duration: 0.0)
        
        //this moves the image run forever and put the action in the correct sequence.
        var moveParallaxForever = SKAction.repeatActionForever(SKAction.sequence([moveParallax, resetParallax]))
        
        
        for var i:CGFloat = 0; i<2 + self.frame.size.width / (texture.size().width); ++i {
            var parallax = SKSpriteNode(texture: texture)
            parallax.position = CGPointMake(i * parallax.size.width, parallax.size.height / 2)
            parallax.runAction(moveParallaxForever)
            self.addChild(parallax)
        }
        

        
        
        self.name = "Parallax"

        self.anchorPoint = CGPoint(x: 0, y: 0)
        self.size.width = self.frame.size.width
        self.size.height = self.frame.size.height

        
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    


    
    
}

