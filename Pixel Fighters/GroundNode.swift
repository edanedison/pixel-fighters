//
//  GroundNode.swift
//  Pixel Fighters
//
//  Created by Rishabh Parikh on 1/12/15.
//  Copyright (c) 2015 Rishabh Parikh. All rights reserved.
//

import Foundation
import SpriteKit

class GroundNode: SKSpriteNode {
    
    init(groundAtPosition: CGPoint) {
        
        var texture = SKTexture(imageNamed: "plane_grassy")
        
        super.init(texture: texture, color: UIColor.redColor(), size: texture.size())
        
        self.position = groundAtPosition
        self.name = "Ground"
        setUpPhysicsBody()
        
    }

    func setUpPhysicsBody(){
        self.physicsBody = SKPhysicsBody(rectangleOfSize: self.frame.size)

        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.dynamic = false
//        self.physicsBody?.categoryBitMask = CollisionCategory.Ground.rawValue
//        self.physicsBody?.collisionBitMask = 0
//        self.physicsBody?.contactTestBitMask = CollisionCategory.Rock.rawValue
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}