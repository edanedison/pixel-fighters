//
//  GamePlayScene.swift
//  Pixel Fighters
//
//  Created by Rishabh Parikh on 1/11/15.
//  Copyright (c) 2015 Rishabh Parikh. All rights reserved.
//

import UIKit
import SpriteKit
import Foundation
import AVFoundation



class GamePlayScene: SKScene, SKPhysicsContactDelegate {

    let gravity = -9.8
    var level = 1
    // Make into an array

    
    
    var lastUpdateTimeInterval: NSTimeInterval?
    var timeSinceEnemyAdded: NSTimeInterval?
    var totalGameTime: NSTimeInterval?
    var minSpeed: Int?
    var addEnemyTimeInterval: NSTimeInterval?
    var maxSpeed: Int?
    var damageSound = SKAction.playSoundFileNamed("Damage.caf", waitForCompletion: false)
    var explodeSound = SKAction.playSoundFileNamed("Explode.caf", waitForCompletion: false)
    var laserSound = SKAction.playSoundFileNamed("Laser.caf", waitForCompletion: false)

    var gameplayMusic: AVAudioPlayer = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Gameplay", withExtension: "mp3"), error: nil)
    var gameOverMusic: AVAudioPlayer = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("GameOver", withExtension: "mp3"), error: nil)
    var gameOver: Bool?
    var gameOverDisplayed: Bool?

    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        self.gameOver = false
        self.gameOverDisplayed = false
        
       self.physicsWorld.contactDelegate = self;
       self.physicsWorld.gravity = CGVector(dx: 0, dy: gravity)
        

        

        
        var background = BackgroundNode(backgroundAtPosition: CGPoint(x: 0, y: 0))
        background.zPosition = 0
        self.addChild(background)
        
        
        var parallax = ParallaxNode(parallaxAtPosition: CGPoint(x: 0, y: 0))
        parallax.zPosition = -1
        self.addChild(parallax)
        

        var ground = GroundNode(groundAtPosition: CGPoint(x: self.frame.midX, y: 0))
        ground.zPosition = 2
        self.addChild(ground)
        
        
        
        var machine = MachineNode(machineAtPosition: CGPoint(x: self.frame.midX, y: 12))
        machine.zPosition = 3
        //self.addChild(machine)

        var spaceCat = SpaceCatNode(catAtPosition: CGPoint(x: machine.position.x, y: 200))
        spaceCat.zPosition = 3
        //self.addChild(spaceCat)


        
        var hero = HeroNode(heroAtPosition: CGPoint(x: size.width / 2 , y: ground.size.height))
        hero.zPosition = 10

        self.addChild(hero)
     
        
        var leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
        var rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
        
        leftSwipe.direction = .Left
        rightSwipe.direction = .Right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        
        

        self.lastUpdateTimeInterval = 0;
        self.timeSinceEnemyAdded = 0
        self.totalGameTime = 0;
        self.minSpeed = Int(Util.SPACEDOG_MIN_SPEED)
        self.maxSpeed = Int(Util.SPACEDOG_MAX_SPEED)
        self.addEnemyTimeInterval = 1.5;

//         self.gameplayMusic.numberOfLoops = 1
//         self.gameplayMusic.prepareToPlay()
//         self.gameplayMusic.volume = 0.2
//         self.gameplayMusic.play()
//
//         self.gameOverMusic.numberOfLoops = 1
//         self.gameOverMusic.prepareToPlay()

        var hudNode = HUDNode(hudAtPosition: CGPointMake(0, self.frame.size.height-20), frame: self.frame)
        hudNode.name = "HUD"
        self.addChild(hudNode)
    }

    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        var hero = self.childNodeWithName("Hero") as HeroNode

    
        
        
        
        let touch = touches.anyObject() as UITouch
        let location = touch.locationInNode(self)
        var multiplierForDirection: CGFloat
        
        println(location)
    
        let heroVelocity = self.frame.size.width / 2.0
        
        let moveDifference = CGPointMake(location.x - hero.position.x, location.y - hero.position.y)
        let distanceToMove = sqrt(moveDifference.x * moveDifference.x + moveDifference.y * moveDifference.y)
        
        let moveDuration = distanceToMove / heroVelocity
        
        if (moveDifference.x < 0) {
            multiplierForDirection = -1.0
        } else {
            multiplierForDirection = 1.0
        }
        
       hero.xScale = fabs(hero.xScale) * multiplierForDirection
        
        
        if (hero.actionForKey("heroIdle") != nil) {
            
            hero.removeActionForKey("heroIdle")
            hero.heroWalking()
            //stop just the moving to a new location, but leave the walking legs movement running
            
        }
        
        if (hero.actionForKey("heroIdle") == nil) {
            hero.removeActionForKey("heroIdle")
            
            //if legs are not moving go ahead and start them
            hero.heroWalking()
            hero.heroPunch()
            // shootProjectileTowardsPosition(CGPointMake(multiplierForDirection * self.frame.size.width, self.frame.midY))

            // hero.removeActionForKey("heroIdle")
        }
        
        
        
        //  let moveAction = (SKAction.moveByX(multiplierForDirection, y: 0, duration: (Double(moveDuration))))
        
        let moveAction = (SKAction.moveToX(location.x, duration:(Double(moveDuration))))
        
        let doneAction = (SKAction.runBlock({
            println("Animation Completed")
            // hero.removeActionForKey("heroWalking")
            hero.heroIdle()
            // hero.heroMoveEnded()
        }))
        
        let moveActionWithDone = (SKAction.sequence([moveAction, doneAction]))
          hero.runAction(moveActionWithDone, withKey:"heroIdle")
        
        

        
        
        if(!gameOver!) {
            for touch: AnyObject in touches {
                shootProjectileTowardsPosition(touch.locationInNode(self))
            }
        }
        else {
            self.gameOverMusic.stop()
            self.removeAllChildren()
            var gamePlayScene = GamePlayScene(fileNamed: "GamePlayScene")
            gamePlayScene.size = self.size
            let transition = SKTransition.fadeWithDuration(1.0)
            self.view?.presentScene(gamePlayScene, transition: transition)
        }
    }
    


    
    

    override func update(currentTime: CFTimeInterval) {
        

        
        
        /* Called before each frame is rendered */
        self.timeSinceEnemyAdded! += currentTime - self.lastUpdateTimeInterval!

        if(self.timeSinceEnemyAdded > addEnemyTimeInterval! && !gameOver!) {
            addSpaceDog()
            self.timeSinceEnemyAdded = 0
            self.totalGameTime! += addEnemyTimeInterval!
        }

        self.lastUpdateTimeInterval = currentTime

        if self.totalGameTime > 40 {
            self.addEnemyTimeInterval = 0.5;
            self.minSpeed = 150;
        } else if self.totalGameTime > 30 {
            self.addEnemyTimeInterval = 0.65
            self.minSpeed = 125;
        } else if self.totalGameTime > 20 {
            self.addEnemyTimeInterval = 0.75
            self.minSpeed = 100
        } else if self.totalGameTime > 10 {
            self.addEnemyTimeInterval = 1.00
            self.minSpeed = 75
        }
        else {
            self.addEnemyTimeInterval = 1.5
            self.minSpeed = 50
        }

        if(self.gameOver! && !gameOverDisplayed!) {
            self.performGameOver()
            gameOverDisplayed = true
        }
    }

    func performGameOver() {
        var gameOver = GameOverNode(atPosition: CGPointMake(self.frame.midX, self.frame.midY))
        self.addChild(gameOver)
        gameOver.performAnimation()
        self.gameplayMusic.stop()
        self.gameOverMusic.play()
    }

    
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        
        
        var hero = self.childNodeWithName("Hero") as HeroNode
        
        
        var initLocation = UISwipeGestureRecognizer.locationOfTouch
        
        if (sender.direction == .Left) {
            println("Swipe Left")
           

        // shootProjectileTowardsPosition(CGPointMake(0, self.frame.midY))
 

       hero.xScale = fabs(hero.xScale) * -1
       

        }
        
        if (sender.direction == .Right) {
       hero.xScale = fabs(hero.xScale) * 1
         //   shootProjectileTowardsPosition(CGPointMake(self.frame.maxX, self.frame.midY))

        }
    }
    


    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }

    func random(#min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }


    func addSpaceDog() {

        self.maxSpeed = self.minSpeed! + 50
        var randomBinary = Util.randomNumberWithinRange(0, max: 2)
        var spaceDogNode: SpaceDogNode
        if(randomBinary == 0) {
            spaceDogNode = SpaceDogNode(spaceDogType: SpaceDogType.SpaceDogTypeA)
        }
        else {
            spaceDogNode = SpaceDogNode(spaceDogType: SpaceDogType.SpaceDogTypeA)
        }

        let xMultiplier = random(min: 0, max: size.width * 1)

        spaceDogNode.position = CGPoint(x: xMultiplier, y: self.frame.size.height + spaceDogNode.size.height)
        spaceDogNode.physicsBody?.velocity = CGVector(dx: 0, dy: -Util.randomNumberWithinRange(UInt32(minSpeed!), max: UInt32(maxSpeed!)))
        
        self.addChild(spaceDogNode)
    }

    func shootProjectileTowardsPosition(position: CGPoint) {
        let projectile = ProjectileNode(projectileAtPosition: position)
        projectile.position = CGPoint(x: self.childNodeWithName("Hero")!.position.x,
                                      y: self.childNodeWithName("Hero")!.position.y)

        self.addChild(projectile)
        //self.runAction(laserSound)
        projectile.moveTowardsPosition(position)
        println(position)
        

    }
    


    func createDebrisAtPosition(position: CGPoint) {
        var numberOfPieces = Int(Util.randomNumberWithinRange(5, max: 20))
        for var i=0; i < numberOfPieces; i++ {
            var randomPiece = Util.randomNumberWithinRange(1, max: 4)
            var imageName = "debri_\(randomPiece)"
            var debris = SKSpriteNode(imageNamed: imageName)
            debris.position = position
            debris.zPosition = 6
            self.addChild(debris)

            debris.physicsBody = SKPhysicsBody(rectangleOfSize: debris.frame.size)
            debris.physicsBody?.categoryBitMask = CollisionCategory.Debris.rawValue
            debris.physicsBody?.contactTestBitMask = 0
            debris.physicsBody?.collisionBitMask = CollisionCategory.Ground.rawValue | CollisionCategory.Debris.rawValue
            debris.name = "debris"

            var positive = Util.randomNumberWithinRange(0, max: 2)
            if positive == 0 {
                debris.physicsBody?.velocity = CGVector(dx: Util.randomNumberWithinRange(0, max: 150), dy: Util.randomNumberWithinRange(150, max: 350))
            }
            else {
                debris.physicsBody?.velocity = CGVector(dx: -Util.randomNumberWithinRange(0, max: 150), dy: Util.randomNumberWithinRange(150, max: 350))
            }

            debris.runAction(SKAction.waitForDuration(2), completion: { () -> Void in
                debris.removeFromParent()
            })
        }


        var explosionPath = NSBundle.mainBundle().pathForResource("Explosion", ofType: "sks")
        var explosion : SKEmitterNode = NSKeyedUnarchiver.unarchiveObjectWithFile(explosionPath!) as SKEmitterNode

        explosion.position = position
        explosion.zPosition = 1
        self.addChild(explosion)

        explosion.runAction(SKAction.waitForDuration(2), completion: { () -> Void in
            explosion.removeFromParent()
        })


    }

    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody, secondBody: SKPhysicsBody

        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }

        if firstBody.categoryBitMask == CollisionCategory.Rock.rawValue && secondBody.categoryBitMask == CollisionCategory.Projectile.rawValue {
            (secondBody.node as ProjectileNode).removeFromParent()
            self.runAction(explodeSound)

            (firstBody.node as SpaceDogNode).health -= 1

            if((firstBody.node as SpaceDogNode).health == -1) {

                self.createDebrisAtPosition(contact.contactPoint)
                (self.childNodeWithName("HUD") as HUDNode).addPoints(50)
            }

            (firstBody.node as SpaceDogNode).checkHealth()
        }
        else if firstBody.categoryBitMask == CollisionCategory.Rock.rawValue && secondBody.categoryBitMask == CollisionCategory.Ground.rawValue {
            (firstBody.node as SpaceDogNode).removeFromParent()
            self.createDebrisAtPosition(contact.contactPoint)
            self.runAction(damageSound)
            var hudNode = (self.childNodeWithName("HUD") as HUDNode)
            hudNode.childNodeWithName("Life\(hudNode.getLives())")?.removeFromParent()
            hudNode.removeLife()
            if(hudNode.getLives() == 0) {
                self.gameOver = true
            }
        }
    }
}
