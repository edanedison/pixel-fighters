//
//  MachineNode.swift
//  Pixel Fighters
//
//  Created by Rishabh Parikh on 1/11/15.
//  Copyright (c) 2015 Rishabh Parikh. All rights reserved.
//

import SpriteKit




class HeroNode: SKSpriteNode {
    
    let idleFrames: [SKTexture] =
    [
        SKTexture(imageNamed: "idle01"),
        SKTexture(imageNamed: "idle02"),
        SKTexture(imageNamed: "idle03"),
        SKTexture(imageNamed: "idle04"),
        SKTexture(imageNamed: "idle05"),
        SKTexture(imageNamed: "idle06")
    ]

    let walkingFrames: [SKTexture] =
    [
        SKTexture(imageNamed: "walk01"),
        SKTexture(imageNamed: "walk02"),
        SKTexture(imageNamed: "walk03"),
        SKTexture(imageNamed: "walk04"),
        SKTexture(imageNamed: "walk05"),
        SKTexture(imageNamed: "walk06"),
        SKTexture(imageNamed: "walk07"),
        SKTexture(imageNamed: "walk08"),
        SKTexture(imageNamed: "walk09"),
        SKTexture(imageNamed: "walk10"),
        SKTexture(imageNamed: "walk11")
    ]
    
    
    let punchFrames: [SKTexture] =
    [
        SKTexture(imageNamed: "punch01"),
        SKTexture(imageNamed: "punch02"),
        SKTexture(imageNamed: "punch03"),
        SKTexture(imageNamed: "punch04"),
        SKTexture(imageNamed: "punch05"),
        SKTexture(imageNamed: "punch06"),
        SKTexture(imageNamed: "punch07"),
        SKTexture(imageNamed: "punch08"),
        SKTexture(imageNamed: "punch09"),
        SKTexture(imageNamed: "punch10")
    ]
    
    var punchSound = SKAction.playSoundFileNamed("Laser.caf", waitForCompletion: false)
    
    
    let right = SKAction.moveByX(64, y: 0, duration: 0.6)
    let left = SKAction.moveByX(-64, y: 0, duration: 0.6)
    let up = SKAction.moveByX(0, y: 64, duration: 0.6)
    let down = SKAction.moveByX(0, y: -64, duration: 0.6)
    
    
    
    


    
    init(heroAtPosition: CGPoint) {

        
        
        var texture = SKTexture(imageNamed: "idle01")
        super.init(texture: texture, color: UIColor.whiteColor(), size: texture.size())


        
        self.name = "Hero"
        
        self.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "idle01"), size: texture.size())
        self.physicsBody?.dynamic = true
        self.physicsBody?.allowsRotation = false
        self.physicsBody?.affectedByGravity = true
        self.physicsBody!.mass = 1

        self.position = heroAtPosition
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.setScale(0.5)
        
         self.heroIdle()
        
        func swipedRight(sender:UISwipeGestureRecognizer){
            self.runAction(right)
        }
        func swipedLeft(sender:UISwipeGestureRecognizer){
            self.runAction(left)
        }
        func swipedUp(sender:UISwipeGestureRecognizer){
            self.runAction(up)
        }
        func swipedDown(sender:UISwipeGestureRecognizer){
            self.runAction(down)
        }
        
        
        


    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func heroIdle() {
// println("idle")
        self.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(idleFrames,
                timePerFrame: 0.1,
                resize: false,
                restore: true)),
            withKey:"heroIdle")
    }
    
    
    
    
    func heroWalking() {
    //    println("walking")
        self.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(walkingFrames,
                timePerFrame: 0.05,
                resize: false,
                restore: true)),
            withKey:"heroWalking")
    }
    
    
    func heroPunch() {
      //  println("punching")
        self.runAction(SKAction.repeatAction(
            SKAction.animateWithTextures(punchFrames,
                timePerFrame: 0.02,
                resize: false,
                restore: true), count: 1),
            withKey:"heroPunch")
        self.runAction(punchSound)
    }
    
    
    func heroMoveEnded() {
        self.removeAllActions()

    }
    
    

    
    
}

